#  debian.df -- to get an official Docker Hub image configured to taste
#  Copyright © 2019-2021  Olaf Meeuwissen
#
#  SPDX-License-Identifier: GPL-3.0-or-later

ARG   TAG
FROM  debian${TAG} AS base
LABEL maintainer="Olaf Meeuwissen <paddy-hack@member.fsf.org>"

#  Tweak the official Docker Hub Debian image configuration to taste.
#
#  This aims at keeping this and derived images as reproducible, small
#  and free as possible.

ARG  RELEASE
RUN  for arch in $(dpkg --print-foreign-architectures); do \
         dpkg --remove-architecture $arch; \
     done \
 &&  > /etc/apt/apt.conf \
 &&  echo 'APT::Install-Recommends "false";'              >> /etc/apt/apt.conf \
 &&  echo 'APT::Install-Suggests   "false";'              >> /etc/apt/apt.conf \
 &&  echo 'APT::AutoRemove::RecommendsImportant "false";' >> /etc/apt/apt.conf \
 &&  echo 'APT::AutoRemove::SuggestsImportant   "false";' >> /etc/apt/apt.conf

#  Install a minimal set of common packages.
#
#  This aims at a set of packages that allows bootstrapping of a clean
#  checkout.  Installation of libtool is deferred to the image that we
#  install the C compiler of choice into (libtool depends on one).

RUN  apt-get update  --quiet \
 &&  DEBIAN_FRONTEND=noninteractive \
     apt-get install --quiet --assume-yes \
             autoconf \
             autoconf-archive \
             automake \
             autopoint \
             ca-certificates \
             gettext \
             git \
             make \
             patch \
             pkg-config \
             python3-minimal \
             libpython3-stdlib \
             wget

#  A minimalistic build environment
#
#  This aims at a setup that allows building of sane-backends in its
#  most minimal configuration.

FROM base AS mini

ENV  CC gcc

RUN  apt-get update  --quiet \
 &&  DEBIAN_FRONTEND=noninteractive \
     apt-get install --quiet --assume-yes \
             libtool \
             $CC

#  A full-blown build environment
#
#  This extends the previous setup to allow building of sane-backends
#  with all of its bells and whistles enabled.

FROM mini AS full

RUN  apt-get update  --quiet \
 &&  DEBIAN_FRONTEND=noninteractive \
     apt-get install --quiet --assume-yes \
             doxygen \
             g++ \
             libavahi-client-dev \
             libcurl4-gnutls-dev \
             libgphoto2-dev \
             libieee1284-3-dev \
             libjpeg-dev \
             libpng-dev \
             libpoppler-glib-dev \
             libsnmp-dev \
             libtiff5-dev \
             libusb-1.0-0-dev \
             libv4l-dev \
             libxml2-dev
