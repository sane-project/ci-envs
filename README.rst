=================================================================
Continuous Integration Environments for SANE Project Repositories
=================================================================

:Author: Olaf Meeuwissen
:Copyright: 2015-2021
:SPDX-License-Identifier: GPL-3.0-or-later


Requirements
------------

- Docker Compose (1.17.0 or later)
- Docker Engine (17.09.0 or later)
- ``make``


TL;DR
-----

::

   make help


Getting Started
---------------

Setup vars as described in `.env` properly.

::

   make bootstrap
   docker-compose run --rm $type make CFLAGS="-g -O0 -Werror"

This creates a ``docker-compose.yml`` file and pulls Docker images
used for the supported build types (if not in your cache already).
At present, the following types are available

- debian-bookworm-mini
   Exercises the build in a very minimalistic setup.  Only the GCC
   C compiler is provided.  No development libraries are installed.

- debian-bookworm-full
   Aims to build a full set of backends with all bells and whistles
   turned on.

- alpine-3.17-musl, alpine-3.18-musl
   Builds a full set of backends against the ``musl`` C library.

- fedora-38-clang, fedora-39-clang
   Builds a full set of backends using the ``clang`` compiler, unless
   told to use ``gcc`` at configure time (via the ``CC`` environment
   variable).  Also includes the ``scan-build`` static code analyzer.

- ubuntu-lunar-dist, ubuntu-mantic-dist
   Aims to build a full set of backends with all bells and whistles
   turned on from a source tarball.

This list, without explanations, is also available from

::

   make list


Creating Your Own Images
------------------------

In case you do not want or cannot pull the images, you can easily
build your own with, for example,

::

   make debian-bullseye-full

Any "parent" images that are not available locally yet will be built
as well (but not tagged).

Building all supported images can be done with

::

   make images

Please note that using ``docker-compose`` to build images may result in
inconsistencies if you have some images cached.  This is a consequence
of image dependencies.
